#!/bin/sh

gst-launch-1.0 -v pipewiresrc ! 'video/x-raw, format=YUY2, framerate=20/1' ! videoconvert ! videoscale ! video/x-raw, height=480, width=560, framerate=20/1 ! vp8enc keyframe-max-dist=50 deadline=1 ! rtpvp8pay ! udpsink port=8888 host=$1
