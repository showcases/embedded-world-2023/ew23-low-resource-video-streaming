SUMMARY = "GTK player with touch screen management"
LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://COPYING;md5=801f80980d171dd6425610833a22dbe6"

SRC_URI = "\
	file://COPYING \
	file://main.c \
	file://meson.build \
	file://receiver.sh \
	file://COLLABORA_02_RGB.png \
	file://media-playback-pause-symbolic.png \
	file://media-playback-start-symbolic.png \
"
S = "${WORKDIR}"

# Modify these as desired
PV = "4.0+collaborademo1"

DEPENDS += "gstreamer1.0 gstreamer1.0-plugins-base gstreamer1.0-plugins-bad gtk+3"

inherit meson pkgconfig

DEMO_DIR = "${prefix}/local/collaborademo"

do_install () {
	install -d ${D}${DEMO_DIR}
	install -m 0755 ${B}/collaborademo-gtk-player ${D}${DEMO_DIR}
	install -m 0755 ${S}/receiver.sh ${D}${DEMO_DIR}
	install -m 0644 ${S}/*.png ${D}${DEMO_DIR}
}
FILES:${PN} += "${DEMO_DIR}"
