/*
 * Copyright (C) 2014-2015 Collabora Ltd.
 *   @author George Kiagiadakis <george.kiagiadakis@collabora.com>
 * Copyright (C) 2019, STMicroelectronics - All Rights Reserved
 *   @author Christophe Priouzeau <christophe.priouzeau@st.com>
 *
 * SPDX-License-Identufier: GPL-2.0+
 *
 * NOTE: inspired from https://github.com/GStreamer/gst-plugins-bad/tree/master/tests/examples/waylandsink
 */

#include <gst/gst.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>

static gchar *graph = NULL;
static gchar *shader_file = NULL;
static gboolean nofullscreen = FALSE;

static GOptionEntry entries[] = {
	{"nofullscreen", 'F', 0, G_OPTION_ARG_NONE, &nofullscreen,
		"Do not put video on fullscreeen", NULL},
	{"graph", 0, 0, G_OPTION_ARG_STRING, &graph, "Gstreamer graph to use", NULL},
	{"shader", 0, 0, G_OPTION_ARG_STRING, &shader_file, "Gstreamer shader graph to use", NULL},

	{NULL}
};

typedef struct
{
	GtkWidget *window_widget;

	GstElement *pipeline;

	GMainLoop *loop;
	guint io_watch_id;

	gchar **argv;
	gint current_uri;             /* index for argv */
} DemoApp;

static void
on_about_to_finish (GstElement * playbin, DemoApp * d)
{
	if (d->argv[++d->current_uri] == NULL)
		d->current_uri = 1;

	g_print ("Now playing %s\n", d->argv[d->current_uri]);
	g_object_set (playbin, "uri", d->argv[d->current_uri], NULL);
}

static void
msg_state_changed (GstBus * bus, GstMessage * message, gpointer user_data)
{
	const GstStructure *s;
	DemoApp *d = user_data;

	s = gst_message_get_structure (message);

	/* We only care about state changed on the pipeline */
	if (s && GST_MESSAGE_SRC (message) == GST_OBJECT_CAST (d->pipeline)) {
		GstState old, new, pending;

		gst_message_parse_state_changed (message, &old, &new, &pending);

		switch (new){
		case GST_STATE_VOID_PENDING:
			g_print("new state: GST_STATE_VOID_PENDING\n");
			break;
		case GST_STATE_NULL:
			g_print("new state: GST_STATE_NULL\n");
			break;
		case GST_STATE_READY:
			g_print("new state: GST_STATE_READY\n");
			break;
		case GST_STATE_PAUSED:
			g_print("new state: GST_STATE_PAUSED\n");
			break;
		case GST_STATE_PLAYING:
			g_print("new state: GST_STATE_PLAYING\n");
			break;
		default:
			break;
		}
	}
}

static gboolean
gstreamer_bus_callback (GstBus * bus, GstMessage * message, void *data)
{
	DemoApp *d = data;

	//g_print ("Got %s message\n", GST_MESSAGE_TYPE_NAME (message));

	switch (GST_MESSAGE_TYPE (message)) {
	case GST_MESSAGE_ERROR:{
		GError *err;
		gchar *debug;

		g_print ("Got %s message\n", GST_MESSAGE_TYPE_NAME (message));

		gst_message_parse_error (message, &err, &debug);
		g_print ("Error: %s\n", err->message);
		g_error_free (err);
		if (debug) {
			g_print ("Debug details: %s\n", debug);
			g_free (debug);
		}
		g_main_loop_quit (d->loop);
		break;
	}

	case GST_MESSAGE_STATE_CHANGED:
		msg_state_changed (bus, message, data);
		break;

	case GST_MESSAGE_EOS:
		/* end-of-stream */
		g_print ("EOS\n");
		g_main_loop_quit (d->loop);
		break;

	default:
		/* unhandled message */
		break;
	}
	return TRUE;
}

static void
button_clicked (GtkWidget *button, gpointer data)
{
  DemoApp *d = data;
  GstState actual_state;
  GtkWidget *image;

  image = gtk_button_get_image (GTK_BUTTON(button));

  g_print ("clicked\n");

  gst_element_get_state(d->pipeline, &actual_state, NULL, -1);
  if (actual_state == GST_STATE_PAUSED) {
    gst_element_set_state (d->pipeline, GST_STATE_PLAYING);
    gtk_image_set_from_file (GTK_IMAGE (image),
        "media-playback-pause-symbolic.png");
  } else {
    gst_element_set_state (d->pipeline, GST_STATE_PAUSED);
    gtk_image_set_from_file (GTK_IMAGE (image),
        "media-playback-start-symbolic.png");
  }
}

static void
build_window (DemoApp * d)
{
	GtkCssProvider* provider;
	GstElement *sink;
	GtkWidget *video_widget;
        GtkWidget *box, *box2, *button, *image, *button_image;;

	/* windows */
	d->window_widget = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW(d->window_widget), "GStreamer Wayland GTK ");
	gtk_window_set_default_size (GTK_WINDOW (d->window_widget), 320, 240);
	g_signal_connect (d->window_widget, "destroy",
			G_CALLBACK (gtk_widget_destroyed), &d->window_widget);
	g_signal_connect_swapped (d->window_widget, "destroy",
			G_CALLBACK (g_main_loop_quit), d->loop);

	if (!nofullscreen)
		gtk_window_fullscreen(GTK_WINDOW(d->window_widget));
	//else {
	//	gtk_window_set_decorated (GTK_WINDOW(d->window_widget), FALSE);
	//}


        box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_container_add(GTK_CONTAINER (d->window_widget), box);

        box2 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
        gtk_box_pack_end (GTK_BOX (box), box2, FALSE, TRUE, 0);


        image = gtk_image_new_from_file("COLLABORA_02_RGB.png");

        button = gtk_button_new();
        button_image = gtk_image_new_from_file ("media-playback-pause-symbolic.png");
        gtk_button_set_image (GTK_BUTTON(button), button_image);
        g_signal_connect (button, "clicked", G_CALLBACK (button_clicked), d);

        gtk_widget_set_size_request (button, 0, 140);
        g_object_set (button, "margin-top", 20, "margin-bottom", 20,
            "margin-start", 20, "margin-end", 20, NULL);
        g_object_set (image, "margin-top", 20, "margin-bottom", 20,
            "margin-start", 20, "margin-end", 20, NULL);

        gtk_box_pack_start (GTK_BOX(box2), image, TRUE, TRUE, 0);
        gtk_box_pack_end (GTK_BOX(box2), button, FALSE, TRUE, 0);


	/* styling background color to black */
	const char *data = "#transparent_bg,GtkDrawingArea {\n"
			"    background-color: rgba (88%, 88%, 88%, 1.0);\n"
			"}";

	provider = gtk_css_provider_new();
	gtk_css_provider_load_from_data(provider, data, -1, NULL);
	gtk_style_context_add_provider(gtk_widget_get_style_context(d->window_widget),
                                   GTK_STYLE_PROVIDER(provider),
                                   GTK_STYLE_PROVIDER_PRIORITY_USER);
	g_object_unref(provider);

	gtk_widget_set_name(d->window_widget, "transparent_bg");

	sink = gst_bin_get_by_name (GST_BIN (d->pipeline), "gtkwsink");
	if (!sink && !g_strcmp0 (G_OBJECT_TYPE_NAME (d->pipeline), "GstPlayBin")) {
		g_object_get (d->pipeline, "video-sink", &sink, NULL);
		if (sink && g_strcmp0 (G_OBJECT_TYPE_NAME (sink), "GstGtkWaylandSink") != 0
				&& GST_IS_BIN (sink)) {
			GstBin *sinkbin = GST_BIN (sink);
			sink = gst_bin_get_by_name (sinkbin, "gtkwsink");
			gst_object_unref (sinkbin);
		}
	}
	g_assert (sink);
	g_assert (!g_strcmp0 (G_OBJECT_TYPE_NAME (sink), "GstGtkWaylandSink"));

	g_object_get (sink, "widget", &video_widget, NULL);
	gtk_widget_set_support_multidevice (video_widget, TRUE);
	gtk_widget_set_vexpand (video_widget, TRUE);

	gtk_box_pack_start (GTK_BOX (box), video_widget, TRUE, TRUE, 0);


	gtk_widget_show_all (d->window_widget);

	g_object_unref (video_widget);
	gst_object_unref (sink);
}


int
main (int argc, char **argv)
{
	DemoApp app = {NULL}, *d = &app;
	GOptionContext *context;
	GstBus *bus;
	GError *error = NULL;
	guint bus_watch_id = 0;
	int ret = 0;

	gtk_init (&argc, &argv);
	gst_init (&argc, &argv);

	context = g_option_context_new ("- waylandsink gtk demo");
	g_option_context_add_main_entries (context, entries, NULL);
	if (!g_option_context_parse (context, &argc, &argv, &error)) {
		g_option_context_free (context);
		goto out;
	}
	g_option_context_free (context);

	if (argc > 1) {
		d->argv = argv;
		d->current_uri = 1;

		d->pipeline = gst_parse_launch ("playbin video-sink=gtkwaylandsink", &error);
		if (error)
			goto out;

		g_object_set (d->pipeline, "uri", argv[d->current_uri], NULL);

		/* enable looping */
		g_signal_connect (d->pipeline, "about-to-finish",
						  G_CALLBACK (on_about_to_finish), d);
	} else {
		if (graph != NULL) {
			if (strstr(graph, "gtkwaylandsink") != NULL) {
				d->pipeline = gst_parse_launch (graph, NULL);
			} else {
				g_print("ERROR: graph does not contain gtkwaylandsink !!!\n");
				ret = 1;
				goto out;
			}
		} else if (shader_file != NULL) {
			gchar *fragment_content;
			GFile *file;
			GstElement *customshader;

			d->pipeline = gst_parse_launch ("v4l2src ! "
				"video/x-raw, format=YUY2, width=320, height=240, framerate=(fraction)15/1 ! "
				"videorate  ! video/x-raw,framerate=(fraction)5/1  ! "
				"queue ! videoconvert ! video/x-raw,format=RGBA ! "
				"queue ! glupload ! queue ! glshader name=customshader ! queue ! gldownload ! "
				"queue ! videoconvert ! "
				"queue ! gtkwaylandsink name=gtkwsink", &error);
			if (error)
				goto out;

			file = g_file_new_for_path (shader_file);
			g_file_load_contents (file, NULL, &fragment_content, NULL, NULL, &error);
			g_object_unref (file);
			if (error)
				goto out;

			customshader = gst_bin_get_by_name(GST_BIN(d->pipeline), "customshader");
			g_assert(customshader);

			//g_print("set fragment, content: \n%s\n", fragment_content);
			g_object_set(customshader, "fragment", fragment_content, NULL);

			gst_object_unref (customshader);
			g_free (fragment_content);
		} else {
			d->pipeline = gst_parse_launch ("videotestsrc pattern=18 "
					"background-color=0x000062FF ! gtkwaylandsink name=gtkwsink",
					&error);
			if (error)
				goto out;
		}
	}

	d->loop = g_main_loop_new (NULL, FALSE);
	build_window (d);

	bus = gst_pipeline_get_bus (GST_PIPELINE (d->pipeline));
	bus_watch_id = gst_bus_add_watch (bus, gstreamer_bus_callback, d);
	gst_object_unref (bus);

	gst_element_set_state (d->pipeline, GST_STATE_PLAYING);

	g_main_loop_run (d->loop);

	gst_element_set_state (d->pipeline, GST_STATE_NULL);

out:
	if (error) {
		g_printerr ("ERROR: %s\n", error->message);
		ret = 1;
	}
	g_clear_error (&error);

	if (bus_watch_id)
		g_source_remove (bus_watch_id);

	gst_clear_object (&d->pipeline);
	if (d->window_widget) {
		g_print ("destroy window\n");
		g_signal_handlers_disconnect_by_data (d->window_widget, d->loop);
		gtk_widget_destroy (d->window_widget);
	}
	g_clear_pointer (&d->loop, g_main_loop_unref);

	g_free(graph);
	g_free(shader_file);

	return ret;
}
