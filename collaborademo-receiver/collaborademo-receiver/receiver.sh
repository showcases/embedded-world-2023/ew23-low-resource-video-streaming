#!/bin/sh
cd /usr/local/collaborademo
./collaborademo-gtk-player --graph 'funnel name=funnel ! rtpjitterbuffer latency=50 ! rtpvp8depay ! decodebin3 ! videoconvert ! identity drop-buffer-flags=corrupted ! gtkwaylandsink name=gtkwsink
   udpsrc port=8888 address=0.0.0.0 caps="application/x-rtp, encoding-name=VP8,clock-rate=90000,payload=96" ! funnel.sink_0
   udpsrc port=8888 address=:: caps="application/x-rtp, encoding-name=VP8,clock-rate=90000,payload=96" ! funnel.sink_1'
