# STM32MP157F-DK2 demo for Embedded World 2023

## Sender

Run the sender.sh script on the source device with the argument being
the IP address of the receiver


## Receiver

### Board setup

Download the image from the artifacts of the last CI job, or see below for
building locally.

The image to be flashed on the SD card is the file called:

```
tmp-glibc/deploy/images/stm32mp15-disco/FlashLayout_sdcard_stm32mp157f-dk2-optee.raw
```

Upstream images and instructions for building them can be found at
https://phabricator.collabora.com/w/st_microelectronics/stm32mp1_startup_guide/

### Manual image compilation

As in `.gitlab-ci.yml`:

First, clone the yocto build tree:

```
repo init -u https://github.com/STMicroelectronics/oe-manifest.git -b refs/tags/openstlinux-5.15-yocto-kirkstone-mp1-v22.11.23
repo sync
```

Then, patch the meta-st-openlinux layer:

```
pushd layers/meta-st/meta-st-openstlinux/
git am $path_to_git_root/0001-Patch-yocto-layer-to-produce-the-collabora-demo-imag.patch
ln -s $path_to_git_root/collaborademo-receiver recipes-samples/collaborademo-receiver
popd
```

Build the image:

```
source ./layers/meta-st/scripts/envsetup.sh --no-ui
bitbake st-image-weston
```

And finally, produce the SD card image:

```
./tmp-glibc/deploy/images/stm32mp15-disco/scripts/create_sdcard_from_flashlayout.sh ./tmp-glibc/deploy/images/stm32mp15-disco/flashlayout_st-image-weston/optee/FlashLayout_sdcard_stm32mp157f-dk2-optee.tsv
```

### (Old) Compile the GTK receiver with the yocto SDK

Download the SDK from the above Phabricator.

To build:
```
# Enter the SDK enviroment
. ../sdk/environment-setup-cortexa7t2hf-neon-vfpv4-ostl-linux-gnueabi
# Run meson
cd collaborademo-receiver/collaborademo-receiver
meson sdk
ninja -C sdk
```

### (Old) Setting up the board

On the build image, copy the demo to the board:
```
scp ../receiver.sh *.png sdk/collaborademo-gtk-player root@<board IP address>:
```


On the board:
```
mv receiver.sh *.png collaborademo-gtk-player /usr/local/collaborademo
chown weston:weston /usr/local/collaborademo/*
chmod ux+ /usr/local/collaborademo/receiver.sh
chmod ux+ /usr/local/collaborademo/collaborademo-gtk-player
rm /usr/local/weston-start-at-startup/*
```

Edit `/etc/xdg/weston/weston.ini` and add the following:
```
[autolaunch]
path=/usr/local/collaborademo/receiver.sh
watch=true
```

Reboot the board

